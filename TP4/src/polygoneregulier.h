#ifndef POLYGONEREGULIER_H
#define POLYGONEREGULIER_H

#include"figuregeometrique.h"
#include"point.h"
class PolygoneRegulier : public FigureGeometrique
{
private:
    int _nbPoints;
    Point * _points;
public:
    PolygoneRegulier(const Couleur & couleur,
                     const Point & centere,
                     int rayon,
                     int nbCotes);
    const void afficher();
    const int getNbPoints();
    const Point & getPoint(int indice) const;
};

#endif // POLYGONEREGULIER_H
