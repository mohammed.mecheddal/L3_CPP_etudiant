#ifndef LIGNE_H
#define LIGNE_H

#include"figuregeometrique.h"
#include"point.h"

class Ligne :public FigureGeometrique
{
    private:
        Point _p0;
        Point _p1;
    public:
        Ligne(const Couleur & couleur,
              const Point & p0,
              const Point & p1 );
        const void afficger() override;
        const Point & getP0()const;
        const Point & getP1()const;
};

#endif // LIGNE_H
