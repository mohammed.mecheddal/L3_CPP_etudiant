#include "ligne.h"

#include <iostream>
#include<vector>
#include"polygoneregulier.h"

int main() {
    std::vector<FigureGeometrique*> figures{
                new Ligne({1,0,0},{0,0},{100,200}),
                new PolygoneRegulier({0,1,0},{100,200},50,5)
    };

    for(FigureGeometrique * f: figures)
        f->afficher();
   return 0;
}

