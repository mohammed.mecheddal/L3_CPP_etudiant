#include "polygoneregulier.h"
#include"figuregeometrique.h"
#include<math.h>
#include<iostream>
PolygoneRegulier::PolygoneRegulier(const Couleur & couleur,
                                    const Point & centere,
                                    int rayon,
                                    int nbCotes):
    FigureGeometrique (couleur),
    _nbPoints(nbCotes)

{
    _points = new Point[_nbPoints];
    for(int i=0; i<_nbPoints;i++){
        double angle = 2*M_PI*i/double(_nbPoints);
        int x = rayon * cos(angle) + + centere._x;
        int y = rayon * sin(angle) + + centere._y;
        _points[i] = {x,y};
    }
}

const void PolygoneRegulier::afficher(){
    const Couleur & c = getCouleur();    std::cout<<"PolygoneRegulier'"
            <<c._r<<"_"<<c._g<<"_"<<c._b;
    for(int i=0;i<_nbPoints;i++){
       const Point & p = _points[i];
        std::cout<<" "<<p._x<<" "<<p._y;
    }
    std::cout<<std::endl;
}
const int PolygoneRegulier::getNbPoints(){
    return _nbPoints;
}
const Point & PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}
